<?php

namespace App\Services;

use External\Foo\Auth\AuthWS as FooLoginService;
use External\Bar\Auth\LoginService as BarLoginService;
use External\Baz\Auth\Authenticator as BazLoginService;
use External\Baz\Auth\Responses\Success;
use External\Foo\Exceptions\AuthenticationFailedException;

class LoginService
{
    public function validateUser(string $login, string $password): bool
    {
        switch ($this->getPrefix($login)) {
            case 'FOO':
                return $this->validateFoo($login, $password);
                break;
            case 'BAR':
                return $this->validateBar($login, $password);
                break;
            case 'BAZ':
                return $this->validateBaz($login, $password);
                break;
            default:
                return false;
                break;
        }
    }

    private function validateFoo(string $login, string $password)
    {
        $fooService = new FooLoginService;
        try {
            if (!$fooService->authenticate($login, $password)) {
                return true;
            }
        } catch (AuthenticationFailedException $e) {
            return false;
        }
    }

    private function validateBar(string $login, string $password)
    {
        $barService = new BarLoginService;
        return $barService->login($login, $password);
    }

    private function validateBaz(string $login, string $password)
    {
        $bazService = new BazLoginService;
        return $bazService->auth($login, $password) == new Success();
    }

    private function getPrefix(string $login): string
    {
        return explode('_', $login)[0];
    }
}
