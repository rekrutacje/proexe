<?php

namespace App\Services;

use DateTimeImmutable;
use Lcobucci\JWT\Token\Plain;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class JWTService
{
    public function createJWT(string $url): string
    {
        $config = Configuration::forUnsecuredSigner();

        $now   = new DateTimeImmutable();
        $token = $config->builder()
            ->issuedBy($url)
            ->permittedFor($url)
            ->identifiedBy('4f1g23a12aa')
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now->modify('+1 minute'))
            ->expiresAt($now->modify('+1 hour'))
            ->getToken($config->signer(), $config->signingKey());

        $token->headers();
        $token->claims();

        return $token->toString();
    }
}
