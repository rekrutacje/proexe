<?php

namespace App\Services;

use External\Bar\Movies\MovieService as BarMovieService;
use External\Baz\Movies\MovieService as BazMovieService;
use External\Foo\Exceptions\ServiceUnavailableException as FooServiceUnavailableException;
use External\Bar\Exceptions\ServiceUnavailableException as BarServiceUnavailableException;
use External\Baz\Exceptions\ServiceUnavailableException as BazServiceUnavailableException;
use External\Foo\Movies\MovieService as FooMovieService;

class MoviesService
{

    public function getMovies(): ?array
    {

        try {
            return array_merge($this->getFooMovies(), $this->getBarMovies(), $this->getBazMovies());
        } catch (FooServiceUnavailableException $e) {
            return null;
        } catch (BarServiceUnavailableException $e) {
            return null;
        } catch (BazServiceUnavailableException $e) {
            return null;
        }
    }

    private function getFooMovies(): array
    {
        $service = new FooMovieService();

        return $service->getTitles();
    }

    private function getBarMovies(): array
    {
        $service = new BarMovieService();

        return array_map(function ($array) {
            return $array['title'];
        }, $service->getTitles()['titles']);
    }

    private function getBazMovies(): array
    {
        $service = new BazMovieService();

        return $service->getTitles()['titles'];
    }
}
