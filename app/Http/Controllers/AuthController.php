<?php

namespace App\Http\Controllers;

use App\Services\JWTService;
use Illuminate\Http\Request;
use App\Services\LoginService;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class AuthController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {

        $request->validate([
            'login' => 'required',
            'password' => 'required'
        ]);

        $jwtService = new JWTService;
        $loginService = new LoginService;

        if ($loginService->validateUser($request->login, $request->password)) {
            return response()->json([
                'token' => $jwtService->createJWT(config('app.url'), config('app.jwt')),
                 'status' => 'success'
                ]);
        }

        return response()->json([
            'status' => 'failure',
        ]);
    }
}
