<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MoviesService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class MovieController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getTitles(Request $request): JsonResponse
    {
        $service = new MoviesService();
        $movies = [];

        for ($i = 0; $i < 5; $i++) {
            $movies = $service->getMovies();
            if (!is_null($movies)) {
                break;
            }
            sleep(1);
        }

        if (is_null($movies)) {
            if (Cache::has('movies')) {
                $movies = json_decode(Cache::get('movies'));
            } else {
                return response()->json([
                    'status' => 'failure'
                ]);
            }
        } else {
            Cache::put('movies', json_encode($movies));
        }

        return response()->json([
            'status' => 'success',
            'titles' => $movies
        ]);
    }
}
